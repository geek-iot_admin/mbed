#include <mbed.h>
#include <rtos.h>
 
DigitalOut g_LED1(LED1);
DigitalOut g_LED2(LED2);
//Serial pc(USBTX, USBRX, 115200);
Serial pc(PD_8, PD_9, 115200);
static void ThreadBody(const void *) 
{
	for (;;)
	{
		g_LED1 = !g_LED1;
		Thread::wait(500);
	}
}
int i = 0;
void callback_ex() {
	// Note: you need to actually read from the serial to clear the RX interrupt
	pc.putc(pc.getc());
	g_LED2 = !g_LED2;
}
int main()
{
	Thread thread(ThreadBody);
	
	pc.attach(&callback_ex);
	for (;;)
	{
		pc.printf("Hello,Mbed!\r\n");
		i++;
		g_LED2 = !g_LED2;	
		Thread::wait(100);
	}
}